const readline = require('readline')
const fs = require('fs')
const path = require('path')

const rl = readline.createInterface({
  output: process.stdout,
  input: process.stdin
})

const readyAnswer = {}
const data = {
  type: 'water || gas',
  value: '000432',
  date: new Date('07-13-2021'),
  name: 'Dmitry'
};


// if we don't have directories -> create them
function mkdirp(filepath) {
  const dirname = path.dirname(filepath);

  if (!fs.existsSync(dirname)) {
     fs.mkdirSync(filepath);
  }
}

mkdirp('dist')
mkdirp('audit')

const fileName = path.join(__dirname, `./dist/answer.json`)

// Date format 13-07-2021
function convertDate() {
  const d = new Date();
  const dd = d.getDate();
  const mm = d.getMonth() + 1;
  const yyyy = d.getFullYear();

  return mm + "-" + dd + "-" + yyyy
}

// add data to the archive
function appendData(type, data) {
  fs.appendFileSync(path.join(__dirname, `/audit/${type}.csv`), `\n${data.date}, ${data.value}, ${data.name}`);
}

// show result on console
function showResult(type) {
  const rs = readline.createInterface({
    input: fs.createReadStream(path.join(__dirname, `/audit/${type}.csv`)),
    output: process.stdout,
    terminal: false
  });

  const displayUsers = {}

  rs.on('line', function (line) {
    const [date, value, user] = line.split(',')

    if (user) {
      const userName = user.trim()

      if (displayUsers[userName]) {
        displayUsers[userName] = {
          data: [...displayUsers[userName].data, `${date} -${value}\n`],
          count: displayUsers[userName].count + 1
        }
      } else {
        displayUsers[userName] = {
          data: [`${date} -${value}`],
          count: 1
        }
      }
    }
  }).on('error', function (e) {
    console.error('Ooops', e)
  });

  rs.on('close', function () {
    console.log(`\nData type: ${type}\n`)

    for (const [key, value] of Object.entries(displayUsers)) {
      console.log(`User ${key} - внес(ла) ${value.count} раз(а) данные: \n${value.data.join("")}\n`);
    }
  });
}

// watch file changes
fs.watch(path.join(__dirname, '/dist'), (eventType, fileName) => {
  const fileWasChanged = path.join(__dirname, '/dist', fileName);
  const content = fs.readFileSync(fileWasChanged)
  const parsedData = JSON.parse(content.toString());

  switch (parsedData.type) {
    case 'gas':
      appendData('gas', parsedData);
      showResult('gas')
      break;
    case 'water':
      appendData('water', parsedData);
      showResult('water')
      break;
  }
})

// create answer/question
function askQuestion(question, keyName) {
  return new Promise((resolve, reject) => {
    rl.question(question, (data) => {
      let content;

      switch (keyName) {
        case 'name': {
          if (!data) {
            reject(new Error('Your name is empty'))
          } else {
            content = data
          }
        }
          break;
        case 'type': {
          if (data !== 'water' && data !== 'gas') {
            reject(new Error('Your type is not water or gas'))
          } else {
            content = data;
          }
        }
          break;
        case 'value': {
          const correctValue = new RegExp('^[0-9]{6}$');
          if (!correctValue.test(data)) {
            reject(new Error('Your date is not valid'))
          } else {
            content = parseInt(data)
          }
          break;
        }
      }

      readyAnswer[keyName] = data
      resolve(data)
    })
  })
}

async function answerQuestion() {
  for (let key in data) {
    if (key === 'date') {
      readyAnswer[key] = convertDate()
    } else {
      await askQuestion(`What is your ${key}: `, key);
    }
  }
  rl.close()

  fs.writeFileSync(fileName, JSON.stringify(readyAnswer, null, 2))
}

answerQuestion()
